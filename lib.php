<?
$token="";
$url="https://api.telegram.org/".$token."/";
define('SALT', '');
define('DBNAME','');
define('DBUSER','');
define('DBPASSWORD','');
define('DAY_MAX_REQUEST_NUMBER','20');

//Справочник статусов
$status_a=array();
$status_a[0]='Не в партии';
$status_a[1]='Член';
$status_a[2]='Сторонник';

//Справочник стейтов
$state_a=array();
$state_a[0]='Выключил сам';
$state_a[1]='Активен';
$state_a[2]='Выключен за разницу в никах';

//Справочник должностей
$position_a=array();
$position_a[0]="Нет должности";
$position_a[1]="Председатель партии";
$position_a[2]="Член ФК";
$position_a[3]="Секретарь ФК";
$position_a[4]="Член ЦКРК";
$position_a[5]="Член ЭК";
$position_a[6]="Федеральный казначей";
$position_a[7]="Член РК";
$position_a[8]="Секретарь РК";
$position_a[9]="Член РКРК";
$position_a[10]="Казначей РО";
$position_a[11]="Заместитель секретаря РО";


function getHash($nick) {
  $s_hash=hash("sha256",strtolower($nick).SALT);
  return $s_hash;
}

function getNickHash($nick) {
  return getHash($nick);
}
