<?
setlocale (LC_ALL, "ru_RU.UTF-8");
include("../lib.php");

$request=file_get_contents('php://input');


$data=array();

if (0) {
  //логирование выключено
  $fn='callbacks.txt';
  $request_log="\n".date('Y-m-d H:i:s')."::";
  $request_log.=$request."|";
  $request_log.="<br>\n";
  $f=fopen($fn,'a');
  echo $request_log;
  fwrite($f,$request_log);
  fclose($f);
}

 function sendMessage($chat_id, $message)
 {
 file_get_contents($GLOBALS['api'] . '/sendMessage?chat_id=' . $chat_id . '&text=' . urlencode($message));
 }


function loadDB() {
  /*
  Формат CSV-файла
  Nickname;Telegram_id;Status;Date;RO;Position;Subposition;Description

  Telegram_id - не используется;
  Description  - пока не используется;
  Обязательные поля: Description и
  */
  global $data;
  $keys=array();
  $data=array();
  $f=file("./db/LPRwiwBot_db.csv");
  foreach ($f as $k=>$v) if ($k==0) {
    $v=trim($v);
    $a=explode(";",$v);
    foreach ($a as $k1=>$v1) {
      $v1=trim($v1);
      if (eregi('nickname',$v1)) {
        $v1="nickname";
      }
      //$v1=eregi_replace("[^a-zA-Z0-9-_]","",$v1);
      $keys[$k1]=$v1;
    }
  } else {
    $a=explode(";",$v);
    $row=array();
    foreach ($a as $k1=>$v1) {
      $row[$keys[$k1]]=trim($v1);
    }
    $nick=getNickHash(strtolower($row['nickname']));
    if (!$data[$nick]['ro']) {
      $data[$nick]=$row;
    } else {
      //var_dump($row);
    }
  }
}

function loadDB2() {
	global $data, $mysqli;
	$mysqli = new mysqli("localhost", DBUSER, DBPASSWORD, DBNAME);
	if ($mysqli->connect_errno) {
    		echo "Не удалось подключиться к MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
	}
	$res = $mysqli->query("SELECT * from user");
        while ($row = $res->fetch_assoc()) {
		$id=$row[id];
	    	if (!$data[$id]['ro']) {
			$data[$id]=$row;
			$data[$id][id]=$row[id];
		}
	}
}

function getInfo($nickname) {
  global $data,$add,$nickname_hash;
  if ($nickname[0]!='@') {
    $nickname='@'.$nickname;
  }
  $nickname_hash=$nickname=getNickHash($nickname);
  if (isset($data[$nickname])) {
	//$add=$data[$nickname][id];
    return $data[$nickname];
  } else {
	//$add=$data[$nickname][id];
    return false;
  }
}

 $api = 'https://api.telegram.org/bot' . $token;


 $output = json_decode(file_get_contents('php://input'), TRUE);

 $chat_id = $output['message']['chat']['id'];
 $first_name = $output['message']['chat']['first_name'];
 $tg_id = $output['message']['chat']['id'];
 $message = trim($output['message']['text']);
 $message_ci=strtolower($message);


loadDB2();
//var_dump($data);
//exit;
$username_=strtolower($output['message']['chat']['username']);

$user=getInfo($username_);

//Ситуации:
//Пользователь зашел, ник есть в базе, tid в базе есть, но tid не совпадает - это другой пользователь занял ник
//Заблокировать вход до разбирательства. Не выдавать по такому пользователю данные.
if ($user[id] && $user[tg_id_hash] && ($user[tg_id_hash]!=getHash($tg_id))) {
  $user['state']=2;
  $sql="update user set state=1 where id='$nickname_hash'";
  //echo $sql."<br>\n";
  $mysqli->query($sql);
}

//Пользователь зашел, ник есть в базе, tid в базе нет - все ок, записать tid в базу
if ($user[id] && !$user[tg_id_hash]) {
  $sql="update user set tg_id_hash='".getHash($tg_id)."' where id='$nickname_hash'";
  //echo $sql."<br>\n";
  $mysqli->query($sql);
}


//Пока не делаю. Пользователь зашел, ника в запросе нет, tid в базе есть - все ок, но данные не отдавать, т.к. у
//него нет ника => о нем не получить данные. Сообщить об этом

//Пользователь зашел, ник есть в базе, tid в базе есть,tid совпадает с указанным - все ок

//Проверка, есть ли пользователь в базе (то есть относится ли он к ЛПР)
if (!$user) {
  $answer="Доступ запрещен".$add;
  sendMessage($chat_id, $answer);
  exit;
}

if ($user['status']!=1 && $user['status']!=2) {
  $answer="Доступ запрещен";
  sendMessage($chat_id, $answer);
  exit;
}

if ($user['state']==0) {
  $answer="Вы запретили показывать информацию о вас, поэтому данные бота вам не доступны. По вопросам обращайтесь в @poutru";
  sendMessage($chat_id, $answer);
  exit;
}

if ($user['state']!=1) {
  $answer="Доступ отключен. По вопросам обращайтесь в @poutru";
  sendMessage($chat_id, $answer);
  exit;
}

if ($message_ci=='/start' || $message_ci=='/help') {
  $answer="LPR Who Is Who Bot предоставляет информацию, принадлежит ли никнейм пользователя Телеграма члену или стороннику ЛПР. Информация выдается только членам или сторонникам ЛПР.
Для этого отправьте никнейм пользователя  Telegram, начинающийся с @. Например: @poutru
В сутки можно отправить не более ".DAY_MAX_REQUEST_NUMBER." запросов.
Если вы не хотите, чтобы по вам предоставлялась информация - отправте команду /off. В этом случае ваши данные не будут показываться в боте, но и вы сами не сможете получать информацию о других пользователях";
  sendMessage($chat_id, $answer);
  exit;
}

if ($message_ci=='/off') {
  $sql="update user set state=0 where id='$nickname_hash'";
  //echo $sql."<br>\n";
  $mysqli->query($sql);
  $answer="Вы запретили показывать информацию о ваc. Данные бота вам более не доступны.";
  sendMessage($chat_id, $answer);
  exit;

}


if ($message[0]=='@' || ($message[0]=='/' && $message[1]=='@')) {
  if ($user[day_request_count]>=DAY_MAX_REQUEST_NUMBER) {
    if (time()-$user['request_date']>24*60*60) {
      $sql="update user set request_date=".time().", day_request_count=0 where id='$nickname_hash'";
      //echo $sql."<br>\n";
      $mysqli->query($sql);
    } else {
      $answer="Вы превысили дневной лимит запросов. Подождите до ".date("d-m-Y H:i",$user['request_date']+24*60*60);
      sendMessage($chat_id, $answer);
      exit;
    }
  }
  $sql="update user set day_request_count=day_request_count+1 where id='$nickname_hash'";
  //echo $sql."<br>\n";
  $mysqli->query($sql);

  $nickname=explode(' ',$message);
  $nickname=strtolower($nickname[0]);
  $nickname=ereg_replace("^\/","",$nickname);

  $nd=getInfo($nickname);
  if (!$nd) {
    $answer=$nickname.". Неизвестно, является ли  членом или сторонником ЛПР.";
  } else {
    //Информацию о членах партии отдаем только членам партии
    if ($user['status']!=1 && $nd['status']==1) {
      $answer=$nickname.". Неизвестно, является ли  членом или сторонником ЛПР.";
      sendMessage($chat_id, $answer);
      exit;
    }

    //Информацию отдаем только для state=1 (то есть пользователь не запретил о себе инфу, есть в активных списках и по нему нет проблем)
    if ($nd['state']!=1) {
      $answer=$nickname.". Неизвестно, является ли  членом или сторонником ЛПР.";
      sendMessage($chat_id, $answer);
      exit;
    }

    if ($nd['status']==0) {
      $answer=$nickname." не относится к ЛПР.";
      sendMessage($chat_id, $answer);
      exit;
    }

    $answer=$nickname." ".$status_a[$nd['status']]." ЛПР";
    //. " state=".$user[state]
/*
Временно отключено, т.к. нет источника данных нормального
    if ($nd['date']) {
      $answer.=" с ".$nd['date']."\n";
    } else {
      $answer.="\n";
    }
    if ($nd['ro']) {
      $answer.="РО ".$nd['ro']."\n";
    }
    if ($nd['position']) {
      $answer.=$position_a[$nd['position']]."\n";
    }
    if ($nd['subposition']) {
      $answer.=$position_a[$nd['subposition']]."\n";
    }
*/
  }
} else {
  $answer="Команда непонятна. Укажите никнейм пользователя  Telegram, начинающийся с @. Например: @test";
}
 sendMessage($chat_id, $answer);
